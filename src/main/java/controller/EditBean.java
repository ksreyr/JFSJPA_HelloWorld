package controller;

import dao.ClientDAO;
import model.Client;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@RequestScoped
@Named
public class EditBean implements Serializable {

    String name, id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id");
    }

    public Client getUser() {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.findbyId(Integer.parseInt(getUserID()));
    }

    public String clientEdit() {
        ClientDAO clientDAO = new ClientDAO();
        Client client1 = clientDAO.findbyId(Integer.parseInt(getUserID()));
        client1.setName(name);
        clientDAO.update(client1);
        return "index?Faces-redirect=true";
    }
}
