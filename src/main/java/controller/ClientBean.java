package controller;

import dao.ClientDAO;
import model.Client;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
//Resive peticiones desde el navegador

/**
 * Diese Klass ist Bean Erzegen,
 * die mit JPA kommunizieren soll
 */
@Named
@RequestScoped
public class ClientBean implements Serializable {

    String username, id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Client> AllClients() {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.findall();
    }

    public String edit(String id) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("id", id);
        return "edit?faces-redirect=true";
    }

    public void erase(String id) {
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.remove(Integer.parseInt(id));
    }

    public String newClient() {
        return "join?faces-redirect=true";
    }


}
