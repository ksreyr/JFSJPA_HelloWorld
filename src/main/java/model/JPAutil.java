package model;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;


/**
 * Hier ist erzeugt die entitty manager,
 * um in die Dao zu nutzen
 */
@Stateless
public class JPAutil{
    private static final String PUName = "jpa";
    private static EntityManagerFactory factory;

    @PersistenceContext(unitName = "jpa", name = "jpa")
    private EntityManager em;

    @Produces
    public EntityManager getEntityManager(){
        return em;
    }
    ////JUST TO PRUBEEEEE ////////
    private static  EntityManager entity;
    public static  EntityManager getEntityManegerP() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory(PUName);
            entity = factory.createEntityManager();
        }
        return entity;
    }
    //////////////------///////////////////
    public static EntityManagerFactory getEntityManegerFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory(PUName);
        }
        return factory;
    }
    public static void shutDown() {
        if (factory != null) {
            factory.close();
        }
    }
}
  /**/
