package controller;

import dao.ClientDAO;
import model.Client;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class JoinBean {
    String id, name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String newOne() {
        Client client = new Client();
        client.setName(name);
        client.setId(Integer.parseInt(id));
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.create(client);
        return "index?Faces-redirect=true";
    }
}
