package dao;

import model.Client;
import model.JPAutil;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import javax.persistence.Query;

import java.util.List;

/**
 * Diese Class ist die verbindung
 * mit die Data base Erzeugen
 */

public class ClientDAO {
    ////////HIER WORKS WOTHOUT PROBLEM
    //EntityManager entity = JPAutil.getEntityManegerP();
    ///////HIER DOES NOT WORK
    @Inject
    private EntityManager entity;

    public void create(Client client) {
        entity.getTransaction().begin();
        entity.persist(client);
        entity.getTransaction().commit();

    }

    public void update(Client client) {
        entity.getTransaction().begin();
        entity.merge(client);
        entity.getTransaction().commit();

    }

    public List<Client> findall() {
        Query q = entity.createQuery("SELECT c FROM Client c");
        List<Client> listaC = q.getResultList();
        return listaC;
    }

    public Client findbyId(int idc) {
        Query q = entity.createQuery("SELECT c FROM Client c WHERE id=" + idc);
        Client C  = (Client) q.getSingleResult();
        return C;
    }

    public void remove(int id) {
        Client c = new Client();
        c = entity.find(Client.class, id);
        entity.getTransaction().begin();
        entity.remove(c);
        entity.getTransaction().commit();
    }
}
