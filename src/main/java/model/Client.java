package model;

import javax.persistence.*;

/**
 * Model der Proyect
 */
@Entity
@Table(name = "Client", uniqueConstraints = {
        @UniqueConstraint(columnNames = "ID")})
public class Client {
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
